package com.codeminders.socketio.server.transport.tomcat;

import com.codeminders.socketio.server.SocketIOServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Mike Nelson (mike@realrunners.com)
 */
public class TomcatSocketIOServlet extends SocketIOServlet {
    @Override
    protected void serve(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        serveSocketIoJs(response);
    }
}
